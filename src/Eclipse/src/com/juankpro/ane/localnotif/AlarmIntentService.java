/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 *  Copyright 2011 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/


package com.juankpro.ane.localnotif;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;


public class AlarmIntentService extends BroadcastReceiver 
{	

	public final static String CLASS_NAME = "com.juankpro.ane.localnotif.AlarmIntentService";
	final static int STANDARD_NOTIFICATION_ID = 0;
	
	public final static String MAIN_ACTIVITY_CLASS_NAME_KEY = "com.juankpro.ane.localnotif.mainActivityClassNameKey";
	
	public final static String NOTIFICATION_CODE_KEY = "com.juankpro.ane.localnotif.notificationCodeKey";
	public final static String ACTION_DATA_KEY = "com.juankpro.ane.localnotif.actionDataKey";
	
	public static final String ICON_RESOURCE = "NOTIF_ICON_RESOURCE";
	public static final String LARGE_ICON_RESOURCE = "NOTIF_LARGE_ICON_RESOURCE";
	public static final String TITLE = "NOTIF_TITLE";
	public static final String BODY = "NOTIF_BODY";
	public static final String NUMBER_ANNOTATION = "NOTIF_NUM_ANNOT";
	public static final String TICKER_TEXT = "NOTIF_TICKER";
	public static final String PLAY_SOUND = "NOTIF_PLAY_SOUND";
	public static final String SOUND_NAME = "NOTIF_SOUND_NAME";
	public static final String VIBRATE = "NOTIF_VIBRATE";
	public static final String CANCEL_ON_SELECT = "NOTIF_CANCEL_OS";
	public static final String REPEAT_UNTIL_ACKNOWLEDGE = "NOTIF_RUA";
	public static final String ON_GOING = "NOTIF_ONGOING";
	public static final String ALERT_POLICY = "NOTIF_POLICY";
	public static final String HAS_ACTION = "NOTIF_HAS_ACTION";
	
    @Override
    public void onReceive(Context context, Intent intent)
    {	
    	Bundle bundle = intent.getExtras();
    	
		//boolean isAppInForeground = isAppInForeground(context);

		//if (!isAppInForeground)
		//{
			if(LocalNotificationManager.freContextInstance == null)
			{
				Log.d("AlarmIntentService::onReceive", "App is not running");
			}
			else
			{
				Log.d("AlarmIntentService::onReceive", "App is running in background");
			}
			
			boolean hasAction = bundle.getBoolean(HAS_ACTION);

			String tickerText = bundle.getString(TICKER_TEXT);
			String title = bundle.getString(TITLE);
			String body = bundle.getString(BODY);
			String code = bundle.getString(NOTIFICATION_CODE_KEY);
			
			NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
				.setContentTitle(title)
				.setContentText(body)
				.setTicker(tickerText);
			setupIcons(context, intent, builder);
			setupDefaults(intent, builder);
			setupMiscellaneous(intent, builder);

			final Intent notificationIntent = new Intent();
			
			if (hasAction)
			{
				String activityClassName = bundle.getString(MAIN_ACTIVITY_CLASS_NAME_KEY);
				byte actionData[] = bundle.getByteArray(ACTION_DATA_KEY);
				
				notificationIntent.setClassName(context, "com.juankpro.ane.localnotif.LocalNotificationIntentService");
				notificationIntent.putExtra(MAIN_ACTIVITY_CLASS_NAME_KEY, activityClassName);
				
				Log.d("AlarmIntentService::onReceive", "Activity Class Name: " + activityClassName);
				
				// Add the notification code of the notification to the intent so we can retrieve it later if the notification is selected by a user.
				notificationIntent.putExtra(NOTIFICATION_CODE_KEY, code);
				
				// Add the action data of the notification to the intent as well.
				notificationIntent.putExtra(ACTION_DATA_KEY, actionData);
			}
			
			final PendingIntent pendingIntent = PendingIntent.getService(context, code.hashCode(), notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			builder.setContentIntent(pendingIntent);

			// Fire the notification.
			final NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(code, STANDARD_NOTIFICATION_ID, builder.build());
		/*}
		else if (LocalNotificationManager.freContextInstance != null)
		{
			LocalNotificationManager.selectedNotificationCode = intent.getStringExtra(NOTIFICATION_CODE_KEY);
			LocalNotificationManager.selectedNotificationData = intent.getByteArrayExtra(ACTION_DATA_KEY);
			LocalNotificationManager.wasNotificationSelected = false;
				
			LocalNotificationManager.freContextInstance.dispatchNotificationSelectedEvent();

			Log.d("AlarmIntentService::onReceive", "App is running in foreground");
		}*/
		
		Log.d("AlarmIntentService::onReceive", "Intent: " + intent.toString());
    }
    
	private void setupIcons(Context context, Intent intent, NotificationCompat.Builder builder)
	{
		final Bundle bundle = intent.getExtras();
		int iconResourceId = bundle.getInt(ICON_RESOURCE);
		int largeIconResourceId = bundle.getInt(LARGE_ICON_RESOURCE);
		int numberAnnotation = bundle.getInt(NUMBER_ANNOTATION);
		
		builder
			.setSmallIcon(iconResourceId)
			.setNumber(numberAnnotation);
		
		if (largeIconResourceId != 0)
		{
			Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), largeIconResourceId);
			builder.setLargeIcon(largeIcon);
		}
	}
	
	private void setupDefaults(Intent intent, NotificationCompat.Builder builder)
    {
		final Bundle bundle = intent.getExtras();
		int defaults = Notification.DEFAULT_LIGHTS;

		boolean playSound = bundle.getBoolean(PLAY_SOUND);
		if (playSound)
		{
			defaults |= Notification.DEFAULT_SOUND;
		}

		boolean vibrate = bundle.getBoolean(VIBRATE);
		if (vibrate)
		{
			defaults |= Notification.DEFAULT_VIBRATE;
		}

		builder.setDefaults(defaults);
	}
	
	private void setupMiscellaneous(Intent intent, NotificationCompat.Builder builder)
	{
		final Bundle bundle = intent.getExtras();
		boolean cancelOnSelect = bundle.getBoolean(CANCEL_ON_SELECT);
		boolean repeatAlertUntilAcknowledged = bundle.getBoolean(REPEAT_UNTIL_ACKNOWLEDGE);
		boolean ongoing = bundle.getBoolean(ON_GOING);
		String alertPolicy = bundle.getString(ALERT_POLICY);
		Log.d("AlarmIntentService::setupMiscellaneous", "cancelOnSelect = " + cancelOnSelect);
		builder.setAutoCancel(cancelOnSelect);
    	//if (repeatAlertUntilAcknowledged)
    	//{
    	//	notification.flags |= Notification.FLAG_INSISTENT;
    	//}
    	// IMPORTANT: The alertPolicy string values map directly to the constants in the NotificationAlertPolicy class in the ActionScript interface.
    	builder.setOnlyAlertOnce(alertPolicy.compareTo("firstNotification") == 0);
    	builder.setOngoing(ongoing);
	}
    
    /*private boolean isAppInForeground(Context context) 
    {
        ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses != null) 
        {
        	final String packageName = context.getPackageName();
        	Log.d("AlarmIntentService::isAppInForeground", "PackageName:" + packageName);
            for (RunningAppProcessInfo appProcess : appProcesses) 
            {
            	if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) 
            	{
            		return true;
            	}
            }
        }
        
        return false;
    }*/
}

